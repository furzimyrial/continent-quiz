import React, { Component } from 'react';
import Header from '../components/header';
import SingleScore from '../components/singlescore';
import styles from './scores.module.css'

class ScoresView extends Component {

  constructor(props){
    super(props);
    this.state = {
      scores: [],
    }
  }

  update(){
    var scores = JSON.parse(localStorage.getItem("scores"));
    var validateScores = function (s) {
      if(s == null)return 0;
      if(s.length == 0)return 0;
      return 1;
    }
    this.setState({scores: (validateScores(scores) == 1) ? scores.map((a,b) => (<SingleScore callBack={this.update.bind(this)} key={b} id={b+1} date={a.date} score={a.score}/>)) : (<div className={styles.noData}>No scores data!</div>)});
  }

  componentDidMount() {
    this.update();
  }

  render () {

    return (
      <React.Fragment>
        <Header val="Scores"/>
        <div className={styles.allScores}>
          {this.state.scores}
        </div>
      </React.Fragment>
    );
  }
}

export default ScoresView;
