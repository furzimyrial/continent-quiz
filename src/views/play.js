import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/header';
import styles from './play.module.css';

class PlayView extends Component {
  render () {
    return (
      <React.Fragment>
        <Header val="Play"/>
        <div className={styles.mainDiv}>
          <div className={styles.centerContent}>
            <div className={styles.iconPart}><i className={"fas fa-globe-europe"}></i></div>
            <div className={styles.buttonsPart}>
              <Link className={styles.buttonDiv} to="/quiz">
                <i className={"fas fa-play"}></i> Start
              </Link>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default PlayView;
