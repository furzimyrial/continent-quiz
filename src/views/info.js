import React, { Component } from 'react';
import Header from '../components/header';
import { Link } from 'react-router-dom';
import styles from './info.module.css';

class InfoView extends Component {
  render () {
    return (
      <React.Fragment>
        <Header val="Back" to="/options"/>
        <div className={styles.aboutDiv}>
          <h1>Continent Quiz</h1>
          <p>No part of this publication may be reproduced, distributed, or transmitted in any form or by any means, including photocopying, recording, or other electronic or mechanical methods, without the prior written permission of the publisher, except in the case of brief quotations embodied in critical reviews and certain other noncommercial uses permitted by copyright law. All Rights Reserved © 2019 Filip Maksimovic</p>
          <i className={styles.faReact+" fab fa-react"}></i>
        </div>
      </React.Fragment>
    );
  }
}

export default InfoView;
