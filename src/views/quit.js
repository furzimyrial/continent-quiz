import React, { Component } from 'react';
import Header from '../components/header';
import styles from './quit.module.css';

class QuitView extends Component {

  render () {
    return (
      <React.Fragment>
        <Header val="Quit"/>
        <div className={styles.quitDiv}>
          Not a native react app sry :(
        </div>
      </React.Fragment>
    );
  }
}

export default QuitView;
