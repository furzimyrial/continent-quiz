import React, { Component } from 'react';
import Header from '../components/header';
import { Link } from 'react-router-dom';
import styles from './result.module.css';

class ResultView extends Component {
  render () {
    console.log(this.props.match.params.result);
    return (
      <React.Fragment>
        <Header val="Results"/>
        <div className={styles.resultDiv}>
          <div className={styles.resultIcon}>
            <i className={"fas fa-laugh-beam"}></i>
          </div>
          <p className={styles.resultText}>Your result is</p>
          <span>{this.props.match.params.result}pts</span>
          <Link className={styles.returnButton} to="/play">Wow cool!</Link>
        </div>
      </React.Fragment>
    );
  }
}

export default ResultView;
