import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/header';
import styles from './options.module.css';

class OptionsView extends Component {

  resetScores() {
      localStorage.setItem("scores","[]");
  }

  render () {
    return (
      <React.Fragment>
        <Header val="Options"/>
        <div className={styles.optionsList}>
          <div onClick={() => this.resetScores()} className={styles.singleOption}>
            Scores reset
          </div>
          <Link to="/info" className={styles.singleOption}>
            About
          </Link>
        </div>
      </React.Fragment>
    );
  }
}

export default OptionsView;
