import React, { Component } from 'react';
import axios from 'axios';
import Header from '../components/header';
import styles from './quiz.module.css'

class QuizView extends Component {
  constructor(props){
    super(props);
    this.state = {
      questions: [],
      currentQuestion: 0,
      questionCount: 5,
      prepareTime: 3,
      questionTime: 5,
      currentTime: 0,
      appState: 'prepare',
      answerDelay: 1,
      score: 0,
      time: 0,
      classes: {
        first: "neutral",
        second: "neutral",
        third: "neutral"
      }
    }
  }


  componentDidMount() {
    //Getting questions
    axios.get(`https://api.myjson.com/bins/a6da9`).then(res => {
      var questions = res.data;
      var randomQuestions = [];
      for(var i = 0; i < 5; i++){
        var randomInt = Math.floor(Math.random() * questions.length);
        var answers = ['Africa','Asia','South America','North America','Europe','Oceania','Antarctica'];
        var answerIdx = answers.indexOf(questions[randomInt].continent);
        answers.splice(answerIdx,1)
        var shuffled = answers.sort(() => 0.5 - Math.random()).splice(0,2);
        shuffled.splice(Math.floor(Math.random() * 3),0,questions[randomInt].continent);
        console.log(shuffled);
        randomQuestions.push({
          image: questions[randomInt].image,
          continent: questions[randomInt].continent,
          answers: shuffled
        });
        questions.splice(randomInt,1);
      }
      console.log(randomQuestions);
      this.setState({questions: randomQuestions});

      //Getting current time in milliseconds
      this.setState({time: new Date()})

      //Setting Timer
      this.interval = setInterval(() => {
        var newTime = Math.floor((new Date() - this.state.time)/1000);
        this.setState({ currentTime: newTime });
        if(this.state.currentTime > 3){
          this.setState({appState: 'playin'});
        }
      }, 1000);
    });


  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  checkQuestion(q) {
    if(this.state.questions[this.state.currentQuestion].continent === this.state.questions[this.state.currentQuestion].answers[q]){
      this.setState({classes: {
          first: (q === 0) ? styles.rightAnswer : styles.neutral,
          second: (q === 1) ? styles.rightAnswer : styles.neutral,
          third: (q === 2) ? styles.rightAnswer : styles.neutral
        },
        score: this.state.score + 750
      });
      setTimeout(() => {
        if(this.state.currentQuestion < 4)
        this.setState({
          currentQuestion: this.state.currentQuestion+1,
          classes: {
            first: styles.neutral,
            second: styles.neutral,
            third: styles.neutral
          }
        });
        else if(this.state.currentQuestion === 4){
          var scores = JSON.parse(localStorage.getItem("scores"));
          var validateScores = function (s) {
            if(s === null)return 0;
            if(s.length === 0)return 0;
            return 1;
          }
          if(validateScores(scores)){
            var today = new Date();
            scores.push({score: this.state.score,date: `${today.getDate()}/${today.getMonth()}/${today.getFullYear()}`});
            var sortedScores = scores.sort((a,b) => (a.score > b.score) ? -1 : 1).slice(0,3);
            localStorage.setItem("scores",JSON.stringify(sortedScores));
          } else {
            var today = new Date();
            var newScores = [{score: this.state.score,date: `${today.getDate()}/${today.getMonth()}/${today.getFullYear()}`}];
            localStorage.setItem("scores",JSON.stringify(newScores));
          }
          this.props.history.push(`/results/${this.state.score}`);
        }
      },this.state.answerDelay*1000);
    } else {
      var theAnswer = this.state.questions[this.state.currentQuestion].answers.indexOf(this.state.questions[this.state.currentQuestion].continent);
      this.setState({classes: {
        first: (q === 0) ? styles.wrongAnswer : (theAnswer === 0) ? styles.rightAnswer : styles.neutral,
        second: (q === 1) ? styles.wrongAnswer : (theAnswer === 1) ? styles.rightAnswer : styles.neutral,
        third: (q === 2) ? styles.wrongAnswer : (theAnswer === 2) ? styles.rightAnswer : styles.neutral
      }});
      setTimeout(() => {
        if(this.state.currentQuestion < 4)
        this.setState({
          currentQuestion: this.state.currentQuestion+1,
          classes: {
            first: styles.neutral,
            second: styles.neutral,
            third: styles.neutral
          }
        });
        else if(this.state.currentQuestion === 4){
          var scores = JSON.parse(localStorage.getItem("scores"));
          var validateScores = function (s) {
            if(s === null)return 0;
            if(s.length === 0)return 0;
            return 1;
          }
          if(validateScores(scores)){
            var today = new Date();
            scores.push({score: this.state.score,date: `${today.getDate()}/${today.getMonth()}/${today.getFullYear()}`});
            var sortedScores = scores.sort((a,b) => (a.score > b.score) ? -1 : 1).slice(0,3);
            localStorage.setItem("scores",JSON.stringify(sortedScores));
          } else {
            var today = new Date();
            var newScores = [{score: this.state.score,date: `${today.getDate()}/${today.getMonth()}/${today.getFullYear()}`}];
            localStorage.setItem("scores",JSON.stringify(newScores));
          }
          this.props.history.push(`/results/${this.state.score}`);
        }
      },this.state.answerDelay*1000);
    }
  }

  render () {
    var headText = (this.state.appState === 'prepare') ? `Prepare in ${this.state.prepareTime - this.state.currentTime}...` : `Question ${this.state.currentQuestion+1} of ${this.state.questionCount}`;
    console.log(this.state.currentQuestion);


    return (
      <React.Fragment>
        <Header val={headText}/>
        <div className={styles.mainDiv}>
          {(this.state.appState !== 'prepare' && this.state.appState !== 'finished') ? (
            <React.Fragment>
              <div className={styles.continentImageDiv}>
                <img className={styles.continentImage} src={this.state.questions[this.state.currentQuestion].image}/>
              </div>
              <button className={styles.questionAnswer+" "+this.state.classes.first} onClick={() => this.checkQuestion(0)}>
                <i className={"fas fa-globe-europe"}></i> {this.state.questions[this.state.currentQuestion].answers[0]}
                <div className={styles.questionCheck}>
                  <i className={"fas fa-times "+styles.faTimes}></i>
                  <i className={"fas fa-check "+styles.faCheck}></i>
                </div>
              </button>
              <button className={styles.questionAnswer+" "+this.state.classes.second} onClick={() => this.checkQuestion(1)}>
                <i className={"fas fa-globe-europe"}></i> {this.state.questions[this.state.currentQuestion].answers[1]}
                <div className={styles.questionCheck}>
                  <i className={"fas fa-times "+styles.faTimes}></i>
                  <i className={"fas fa-check "+styles.faCheck}></i>
                </div>
              </button>
              <button className={styles.questionAnswer+" "+this.state.classes.third} onClick={() => this.checkQuestion(2)}>
                <i className={"fas fa-globe-europe"}></i> {this.state.questions[this.state.currentQuestion].answers[2]}
                <div className={styles.questionCheck}>
                  <i className={"fas fa-times "+styles.faTimes}></i>
                  <i className={"fas fa-check "+styles.faCheck}></i>
                </div>
              </button>
            </React.Fragment>
          ) : ""}
        </div>
      </React.Fragment>
    );
  }
}

export default QuizView;
