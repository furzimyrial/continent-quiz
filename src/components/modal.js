import React, { Component } from 'react';
import styles from './modal.module.css';

class Modal extends Component {

  render () {
    return (
      <div className={styles.modalDiv}>
        <p className={styles.headingText}><i className={"fas fa-angle-right"}></i> {this.props.val}</p>
      </div>
    );
  }
}

export default Modal;
