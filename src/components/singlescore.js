import React, { Component } from 'react';
import styles from './singlescore.module.css';


class SingleScore extends Component {

  deleteScore(id) {
    var scores = JSON.parse(localStorage.getItem("scores"));
    scores.splice(id-1,1);
    localStorage.setItem("scores",JSON.stringify(scores));
    this.props.callBack();
  }

  render () {
    return (
      <div className={styles.scoreDiv}>
        <div className={styles.scoreId}>{this.props.id}#</div>
        <div className={styles.scoreInfo}>
          <p className={styles.scoreDate}>on {this.props.date}</p>
          <p className={styles.scoreValue}>{this.props.score}pts</p>
        </div>
        <a className={styles.scoreDelete} onClick={() => this.deleteScore(this.props.id)}>
          <i className={"fas fa-trash"}></i>
        </a>
      </div>
    );
  }
}

export default SingleScore;
