import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import styles from './navbar.module.css';


class Navbar extends Component {
  render () {
    return (
      <div className={styles.nav}>
        <div className={styles.navDivItem}>
          <NavLink to="/play" className={styles.navItem} activeStyle={{color: "var(--c3)"}}>
            <i className={"fas fa-gamepad"}></i>
            <p>Play</p>
          </NavLink>
        </div>
        <div className={styles.navDivItem}>
          <NavLink to="/scores" className={styles.navItem} activeStyle={{color: "var(--c3)"}}>
            <i className={"fas fa-trophy"}></i>
            <p>Scores</p>
          </NavLink>
        </div>
        <div className={styles.navDivItem}>
          <NavLink to="/options" className={styles.navItem} activeStyle={{color: "var(--c3)"}}>
            <i className={"fas fa-cog"}></i>
            <p>Options</p>
          </NavLink>
        </div>
        <div className={styles.navDivItem}>
          <NavLink to="/quit" className={styles.navItem} activeStyle={{color: "var(--c3)"}}>
            <i className={"fas fa-power-off"}></i>
            <p>Quit</p>
          </NavLink>
        </div>

      </div>
    );
  }
}

export default Navbar;
