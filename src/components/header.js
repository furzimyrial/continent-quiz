import React, { Component } from 'react';
import styles from './header.module.css';
import { Link } from 'react-router-dom';

class Header extends Component {

  render () {
    return (
      <div className={styles.headingDiv}>
        {(this.props.to) ? (<Link to={this.props.to} className={styles.headingText}><i className={"fas fa-angle-left"}></i> {this.props.val}</Link>) : (<p className={styles.headingText}><i className={"fas fa-angle-right"}></i> {this.props.val}</p>)}
      </div>
    );
  }
}

export default Header;
