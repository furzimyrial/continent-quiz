import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import PlayView from './views/play';
import ScoresView from './views/scores';
import OptionsView from './views/options';
import InfoView from './views/info';
import QuizView from './views/quiz';
import QuitView from './views/quit';
import ResultView from './views/result';
import Navbar from './components/navbar';
import './App.css';


function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div className="MainContainer">
          <Switch>
            <Route path="/play" exact component={PlayView}/>
            <Route path="/scores" exact component={ScoresView}/>
            <Route path="/options" exact component={OptionsView}/>
            <Route path="/info" exact component={InfoView}/>
            <Route path="/quiz" exact component={QuizView}/>
            <Route path="/quit" exact component={QuitView}/>
            <Route path="/results/:result" exact component={ResultView}/>
            <Redirect to="/play"/>
          </Switch>
        </div>
        <Route path="/(play|scores|options)/" component={Navbar}/>
      </div>
    </BrowserRouter>
  );
}

export default App;
